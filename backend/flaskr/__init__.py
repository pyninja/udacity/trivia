from flask import Flask, request, abort, jsonify
from flask_cors import CORS
import random
from models import setup_db, Question, Category

QUESTIONS_PER_PAGE = 10


def paginate_result(request_obj, selection):
    page = request_obj.args.get('page', 1, type=int)
    start = (page - 1) * QUESTIONS_PER_PAGE
    end = start + QUESTIONS_PER_PAGE
    contents = [content.format() for content in selection]
    current_contents = contents[start:end]
    return current_contents


def create_app(test_config=None):
    """create and configure the app"""
    app = Flask(__name__)
    setup_db(app)
    # Set up CORS.
    CORS(app)

    @app.after_request
    def after_request(response):
        """Use the after_request decorator to set Access-Control-Allow. Allow * for origin.
        """
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization,true')
        response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
        return response

    @app.route('/categories')
    def fetch_categories():
        """endpoint to handle GET requests for all available categories"""
        category_obj = Category.query.order_by(Category.id).all()
        category_dict = {category.id: category.category for category in category_obj}
        return jsonify({
            'success': True,
            'categories': category_dict,
            'total_categories': len(category_dict)
        })

    @app.route("/categories", methods=["POST"])
    def insert_category():
        """Endpoint to handle POST request to create a new category"""
        body = request.get_json()
        message = body.get("category")
        print(message)
        if message:
            search_category = Category.query.filter(Category.category.ilike('%{}%'.format(message))).one_or_none()
            if search_category:
                abort(422)
            else:
                try:
                    new_category = Category(category=message)
                    new_category.insert()

                    return jsonify({
                        'success': True,
                        'created': new_category.id,
                        'total_categories': len(Category.query.all())
                    })
                except Exception as e:
                    print(e)
                    abort(422)
        else:
            abort(422)

    @app.route('/questions')
    def fetch_questions():
        """An endpoint to handle GET requests for questions."""
        all_questions = Question.query.order_by(Question.id).all()
        current_questions = paginate_result(request, all_questions)
        all_categories = Category.query.order_by(Category.id).all()
        category_dict = {category.id: category.category for category in all_categories}

        if len(current_questions) == 0:
            abort(404)

        return jsonify({
            'success': True,
            "questions": current_questions,
            "total_questions": len(Question.query.all()),
            "categories": category_dict,
            "current_category": None
        })

    @app.route("/questions/<int:question_id>", methods=["DELETE"])
    def delete_question(question_id):
        """An endpoint to DELETE question using a question ID"""
        try:
            question = Question.query.get(question_id)
            if question is None:
                abort(404)
            question.delete()
            return jsonify({
                "success": True,
                "deleted": question_id,
                "total_questions": len(Question.query.all())
            })

        except Exception as e:
            print(e)
            abort(422)

    @app.route("/questions", methods=["POST"])
    def add_question():
        """
        an endpoint to POST a new question, which will require the question and answer text,
        category, and difficulty score. Having a search in the request header it is possible to
        fetch the questions with matches the search term.
        """
        body = request.get_json()

        new_question = body.get("question")
        new_answer = body.get("answer")
        new_category = body.get("category")
        new_difficulty = body.get("difficulty")
        search = body.get("searchTerm")

        try:
            if search:
                search_result = Question.query.filter(Question.question.ilike('%{}%'.format(search))).all()
                return jsonify({
                    "success": True,
                    "questions": [question.format() for question in search_result],
                    "total_questions": len(search_result),
                    "current_category": None
                })
            else:
                new_item = Question(
                    question=new_question, answer=new_answer,
                    category=new_category, difficulty=new_difficulty
                )
                new_item.insert()

                return jsonify({
                    "success": True,
                    "created": new_item.id,
                    "total_questions": len(Question.query.all())
                })
        except Exception as e:
            print(e)
            abort(422)

    @app.route("/questions/search", methods=["POST"])
    def search_question():
        """A post endpoint Take a search term and look in to the database of questions."""
        body = request.get_json()
        search = body.get("searchTerm")
        if search:
            search_result = Question.query.filter(Question.question.ilike('%{}%'.format(search))).all()
            if len(search_result) > 0:
                return jsonify({
                    "success": True,
                    "questions": [question.format() for question in search_result],
                    "total_questions": len(search_result),
                    "current_category": None
                })
            else:
                abort(422)
        else:
            abort(422)

    @app.route("/categories/<int:category_id>/questions")
    def fetch_question_by_category(category_id):
        """A GET endpoint to get questions based on category"""
        try:
            question_obj = Question.query.filter(Question.category == category_id).all()
            if len(question_obj) > 0:
                return jsonify({
                    "success": True,
                    "questions": [question.format() for question in question_obj],
                    "total_questions": len(question_obj),
                    "current_category": category_id
                })
            else:
                abort(404)
        except Exception as e:
            print(e)
            abort(404)

    @app.route('/quizzes', methods=['POST'])
    def play_quiz():
        """A POST endpoint to get questions to play the quiz"""
        try:
            body = request.get_json()

            if not ('quiz_category' in body and 'previous_questions' in body):
                abort(422)

            body_category = body.get('quiz_category')
            body_question = body.get('previous_questions')

            if body_category['type'] == 'click':
                question_obj = Question.query.filter(Question.id.notin_(body_question)).all()
            else:
                question_obj = (
                    Question.query
                    .filter_by(category=body_category['id'])
                    .filter(Question.id.notin_(body_question)).all()
                )

            new_question = (
                question_obj[random.randrange(0, len(question_obj))].format()
                if len(question_obj) > 0 else None
            )

            return jsonify({
                'success': True,
                'question': new_question
            })
        except Exception as e:
            print(e)
            abort(422)

    @app.errorhandler(404)
    def not_found(error):
        """error handlers for not found."""
        return jsonify({
          "success": False,
          "error": 404,
          "message": "resource not found"
          }), 404

    @app.errorhandler(422)
    def unprocessable(error):
        """Error handler for request we can not process"""
        return jsonify({
          "success": False,
          "error": 422,
          "message": "unprocessable"
          }), 422

    @app.errorhandler(400)
    def bad_request(error):
        """Error handler for client side bad request"""
        return jsonify({
          "success": False,
          "error": 400,
          "message": "bad request"
          }), 400

    return app
