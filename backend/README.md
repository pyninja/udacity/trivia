# Full Stack Trivia API Backend

## Getting Started
The front end and back end is separated for the application. All the server side request
are in the backend written in Python. 

### Installing Dependencies

#### Python 3.7
Follow instructions to install the latest version of python for your platform in the 
[python docs](https://docs.python.org/3/using/unix.html#getting-and-installing-the-latest-version-of-python)

#### Virtual Enviornment
We recommend working within a virtual environment whenever using Python for projects. 
This keeps your dependencies for each project separate and organaized. Instructions 
for setting up a virual enviornment for your platform can be found in the 
[python docs](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/)

#### PIP Dependencies
Once you have your virtual environment setup and running, install dependencies 
by naviging to the `/backend` directory and running:

```bash
pip install -r requirements.txt
```

This will install all of the required packages we selected within the `requirements.txt` 
file. If you are using conda environment some package might not be available in conda
core repository. In that case you can install the package using `pip` or `conda forge`.

##### Key Dependencies
- [Flask](http://flask.pocoo.org/)  is a lightweight backend microservices framework. 
    Flask is required to handle requests and responses.

- [SQLAlchemy](https://www.sqlalchemy.org/) is the Python SQL toolkit and ORM we'll 
    use handle the lightweight sqlite database. You'll primarily work in app.py and 
    can reference models.py. 

- [Flask-CORS](https://flask-cors.readthedocs.io/en/latest/#) is the extension we'll 
    use to handle cross origin requests from our frontend server. 

## Database Setup
_Note: Schema has been used, instead of a default schema, you might have a look
on the modles file_
With Postgres running, restore a database using the trivia.psql file provided.
From the backend folder in terminal run:

```bash
psql <user_name>
\connect udacity;
\create schema trivia;
```
Now running the backend app should create all the tables under trivia schema.
Then we can run the following command from `psql` interface to insert the data.
Please replace `<...your file location...>` with your own location.

```bash
\copy trivia.categories(id, category) from /home/<...your file location...>/backend/helper/categories.tsv with delimiter as e'\t';
\copy trivia.questions(question, answer, difficulty, category) from /home/<...your file location...>/backend/helper/questions.tsv with delimiter as e'\t';
```

Alternatively you can use `trivia.psql`, but you need to  customise the schema name to 
generate the tables and insert data.

## Running the server

From within the `backend` directory first ensure you are working using your created 
virtual environment. To run the server, execute:

```bash
export FLASK_APP=flaskr
export FLASK_ENV=development
flask run
```

Setting the `FLASK_ENV` variable to `development` will detect file changes and 
restart the server automatically.

Setting the `FLASK_APP` variable to `flaskr` directs flask to use the `flaskr` 
directory and the `__init__.py` file to find the application. 

## Tasks

One note before you delve into your tasks: for each endpoint you are expected to define the endpoint and response data. The frontend will be a plentiful resource because it is set up to expect certain endpoints and response data formats already. You should feel free to specify endpoints in your own way; if you do so, make sure to update the frontend or you will get some unexpected behavior. 

1. Use Flask-CORS to enable cross-domain requests and set response headers. 
2. Create an endpoint to handle GET requests for questions, including pagination (every 10 questions). This endpoint should return a list of questions, number of total questions, current category, categories. 
3. Create an endpoint to handle GET requests for all available categories. 
4. Create an endpoint to DELETE question using a question ID. 
5. Create an endpoint to POST a new question, which will require the question and answer text, category, and difficulty score. 
6. Create a POST endpoint to get questions based on category. 
7. Create a POST endpoint to get questions based on a search term. It should return any questions for whom the search term is a substring of the question. 
8. Create a POST endpoint to get questions to play the quiz. This endpoint should take category and previous question parameters and return a random questions within the given category, if provided, and that is not one of the previous questions. 
9. Create error handlers for all expected errors including 400, 404, 422 and 500. 

## API Referances

### Getting Started
-   Base URL: At present this app can only be run locally and is not hosted as a 
    base URL. The backend app is hosted at the default, http://127.0.0.1:5000/, 
    which is set as a proxy in the frontend configuration.
-   Authentication: This version of the application does not require authentication 
    or API keys.

### Error Handling
Errors are returned as JSON objects in the following format:

```
{
    "success": False, 
    "error": 400,
    "message": "bad request"
}
```
The API will return three error types when requests fail:
-   400: Bad Request
-   404: Resource Not Found
-   422: Not Processable

### Endpoints

#### Get /categories
- General:
    -   Returns all available categories for questions
-   Sample: `curl http://127.0.0.1:5000/categories`
```
{
  "categories": {
    "1": "Science", 
    "2": "Art", 
    "3": "Geography", 
    "4": "History", 
    "5": "Entertainment", 
    "6": "Sports", 
    "8": "Tech"
  }, 
  "success": true, 
  "total_categories": 7
}
```
#### POST /categories
- General:
    - Insert a new category to the database and return success feed in case of
        successful operation.
- Sample: `curl -X POST -H "Content-Type: application/json" -d '{"category": "Culture"}' http://127.0.0.1:5000/categories`

```
{
  "created": 9, 
  "success": true, 
  "total_categories": 8
}

```
Please set the autoincrement for the primary key in postgres if you encounter any
error, as posgtres does not set it by default.
```
SELECT setval(pg_get_serial_sequence('trivia.categories', 'id'), coalesce(max(id),1), false) FROM  trivia.categories;
```

#### GET /questions
- General:
    - Get all the questions available with pagination, also except a page parameter
        other wise return page 1 always
- Sample: `curl http://127.0.0.1:5000/questions?page=1`

```
{
  "categories": {
    "1": "Science", 
    "2": "Art", 
    "3": "Geography", 
    "4": "History", 
    "5": "Entertainment", 
    "6": "Sports", 
    "8": "Tech", 
    "9": "Culture"
  }, 
  "current_category": null, 
  "questions": [
    {
      "answer": "Maya Angelou", 
      "category": 4, 
      "difficulty": 2, 
      "id": 1, 
      "question": "Whose autobiography is entitled 'I Know Why the Caged Bird Sings'?"
    }, 
    {
      "answer": "Muhammad Ali", 
      "category": 4, 
      "difficulty": 1, 
      "id": 2, 
      "question": "What boxer's original name is Cassius Clay?"
    }, 
    {
      "answer": "Apollo 13", 
      "category": 5, 
      "difficulty": 4, 
      "id": 3, 
      "question": "What movie earned Tom Hanks his third straight Oscar nomination, in 1996?"
    }, 
    {
      "answer": "Tom Cruise", 
      "category": 5, 
      "difficulty": 4, 
      "id": 4, 
      "question": "What actor did author Anne Rice first denounce, then praise in the role of her beloved Lestat?"
    }, 
    {
      "answer": "Edward Scissorhands", 
      "category": 5, 
      "difficulty": 3, 
      "id": 5, 
      "question": "What was the title of the 1990 fantasy directed by Tim Burton about a young man with multi-bladed appendages?"
    }, 
    {
      "answer": "Brazil", 
      "category": 6, 
      "difficulty": 3, 
      "id": 6, 
      "question": "Which is the only team to play in every soccer World Cup tournament?"
    }, 
    {
      "answer": "Uruguay", 
      "category": 6, 
      "difficulty": 4, 
      "id": 7, 
      "question": "Which country won the first ever soccer World Cup in 1930?"
    }, 
    {
      "answer": "George Washington Carver", 
      "category": 4, 
      "difficulty": 2, 
      "id": 8, 
      "question": "Who invented Peanut Butter?"
    }, 
    {
      "answer": "Lake Victoria", 
      "category": 3, 
      "difficulty": 2, 
      "id": 9, 
      "question": "What is the largest lake in Africa?"
    }, 
    {
      "answer": "The Palace of Versailles", 
      "category": 3, 
      "difficulty": 3, 
      "id": 10, 
      "question": "In which royal palace would you find the Hall of Mirrors?"
    }
  ], 
  "success": true, 
  "total_questions": 21
}
```

#### DELETE /questions
- General:
    - Make a delete request to delete any quesiton by its ID, ID parameter must be
        an Integer
- Sample: `curl -X DELETE http://127.0.0.1:5000/questions/8`

_Test has been done using frontend and output is not added here._

#### POST /questions
- General:
    - Post request to create a new question, also provide search option to search
        questions by keyword
- Sample: 
    -Insert a new question: `curl -X POST -H "Content-Type: application/json" -d '{"question": "Who invented stochastic gradient descent?", "answer": "Robbins and Monro", "category": 1, "difficulty": 4}' http://127.0.0.1:5000/questions`
    - Search question by keyword: `curl -X POST -H "Content-Type: application/json" -d '{"searchTerm": "Who"}' http://127.0.0.1:5000/questions`
_Test has been done using frontend and output is not added here._
#### GET //categories/<ID>/questions
- General:
    - Get all the quesitons by given category ID, ID must be an integer
- Sample: `curl -X GET http://127.0.0.1:5000/categories/1/questions`

```
{
  "current_category": 1, 
  "questions": [
    {
      "answer": "The Liver", 
      "category": 1, 
      "difficulty": 4, 
      "id": 16, 
      "question": "What is the heaviest organ in the human body?"
    }, 
    {
      "answer": "Alexander Fleming", 
      "category": 1, 
      "difficulty": 3, 
      "id": 17, 
      "question": "Who discovered penicillin?"
    }, 
    {
      "answer": "Blood", 
      "category": 1, 
      "difficulty": 4, 
      "id": 18, 
      "question": "Hematology is a branch of medicine involving the study of what?"
    }, 
    {
      "answer": "Robbins and Monro", 
      "category": 1, 
      "difficulty": 4, 
      "id": 25, 
      "question": "Who invented stochastic gradient descent?"
    }, 
    {
      "answer": "Al Kharezmi", 
      "category": 1, 
      "difficulty": 3, 
      "id": 30, 
      "question": "Who is the author of the book Al Zabr Wal Mukabela?"
    }
  ], 
  "success": true, 
  "total_questions": 5
}

```
#### POST /quizzes
- General:
    - The post request to play the quiz, can be accessible from the frontend 
        of the app

_Test has been conducted with the frontend_

## Testing
After set up the database and insert the sample data it is possible to run the
unit test pack. The data insert test has been disable as the duplicate constrain
has been applied the data base so running those test more than once will fail the
tests.

```
dropdb trivia_test
createdb trivia_test
psql trivia_test < trivia.psql
python test_flaskr.py
```