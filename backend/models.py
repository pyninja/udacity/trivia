import os
from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy import ForeignKey
from flask_sqlalchemy import SQLAlchemy
import json

# database_name = "trivia"
database_uri = 'postgres://datapsycho:admin1@localhost:5432/udacity'

db = SQLAlchemy()


# setup_db(app) binds a flask application and a SQLAlchemy service
def setup_db(app, database_path=database_uri):
    app.config["SQLALCHEMY_DATABASE_URI"] = database_path
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.app = app
    db.init_app(app)
    db.create_all()


# Category
class Category(db.Model):
    __tablename__ = 'categories'
    __table_args__ = {"schema": "trivia"}

    id = Column(Integer, primary_key=True)
    category = Column(String, nullable=False, unique=True)
    questions = db.relationship('Question', backref='category_', lazy=True)

    def __init__(self, category):
        self.category = category

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def format(self):
        return {
            'id': self.id,
            'category': self.category
        }


# Question
class Question(db.Model):
    __tablename__ = 'questions'
    __table_args__ = {"schema": "trivia"}
    id = Column(Integer, primary_key=True)
    question = Column(String)
    answer = Column(String)
    category = Column(Integer, db.ForeignKey('trivia.categories.id'), nullable=False)
    difficulty = Column(Integer)

    def __init__(self, question, answer, category, difficulty):
        self.question = question
        self.answer = answer
        self.category = category
        self.difficulty = difficulty

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def format(self):
        return {
          'id': self.id,
          'question': self.question,
          'answer': self.answer,
          'category': self.category,
          'difficulty': self.difficulty
        }
